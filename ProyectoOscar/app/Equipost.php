<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipost extends Model
{
    protected $table = 'equipos';

    protected $fillable = [
        'nom_equipo', 'jugador'
    ];
}
